namespace Extensions
{
    public static class ArrayExtensions
    {
        public static int IndexOf<T>(this T[] array, T member)
        {
            for (var i = 0; i < array.Length; i++)
                if (array[i].Equals(member))
                    return i;

            return -1;
        }
    }
}