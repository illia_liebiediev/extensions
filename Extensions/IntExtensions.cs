using System.Linq;

namespace Extensions
{
    public static class IntExtensions
    {
        public static bool IsTheBiggestOf(this int number, params int[] others) =>
            others.All(other => number > other);
            
        public static bool IsTheSmallestOf(this int number, params int[] others) =>
            others.All(other => number < other);

        public static bool AllEqual(this int number, params int[] others) =>
            others.All(other => number == other);

        public static bool IsEven(this int controlsCount) =>
            controlsCount % 2 == 0;
    }
}