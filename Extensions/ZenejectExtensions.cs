﻿namespace Extensions;

public class ZenejectExtensions
{
    public static DiContainer BindSingleNonLazyService<TImplementation>(this DiContainer container)
    {
        container.Bind<TImplementation>().AsSingle().NonLazy();
        return container;
    }
        
    public static DiContainer BindSingleNonLazyService<TInterface, TImplementation>(this DiContainer container)
        where TImplementation : TInterface
    {
        container.Bind<TInterface>().To<TImplementation>().AsSingle().NonLazy();
        return container;
    }

    public static DiContainer BindSingleNonLazyService<T>(this DiContainer container, params Type[] args)
    {
        container.Bind(args).To<T>().AsSingle().NonLazy();
        return container;
    } }