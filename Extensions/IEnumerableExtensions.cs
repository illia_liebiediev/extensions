﻿using System.Collections.Generic;
using System.Linq;

namespace Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<string> ClearEmptyLine(this List<string> list)
        {
            list.RemoveAll(v => v == " " || v == "");
            return list;
        }

        public static IEnumerable<string> ClearDash(this List<string> list)
        {
            list.RemoveAll(v => v == "-");
            return list;
        }

        public static IEnumerable<string> ClearUnnecessaryCharacters(this List<string> list)
        {
            ClearEmptyLine(list);
            ClearDash(list);
            return list;
        }

        public static List<T> Random<T>(this IEnumerable<T> collection, int amount)
        {
            var randomList = new List<T>();
            for (var i = 0; i < amount; i++)
            {
                var rand = UnityEngine.Random.Range(0, collection.Count());
                var element = collection.ElementAt(rand);
                randomList.Add(element);
            }

            return randomList;
        }



        public static IEnumerable<T> RandomWithOutRepeatElement<T>(this IEnumerable<T> collection, int amount)
        {
            var resultCollection = new List<T>();

            for (var i = 0; i < amount; i++)
            {
                var rand = UnityEngine.Random.Range(0, collection.Count());
                var element = collection.ElementAt(rand);
                if (resultCollection.Contains(element))
                {
                    i--;
                    continue;
                }

                resultCollection.Add(element);
            }

            return resultCollection;
        }

        public static T RandomFromCollection<T>(this IEnumerable<T> collection)
        {
            var rand = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAt(rand);
        }

        public static void ForEach(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var element in collection)
            {
                action(element);
            }
        }
    }
}